package nl.lolmewn.backtobody;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class EventListener implements Listener {

    private final Main plugin;

    EventListener(Main main) {
        this.plugin = main;
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }

        final Player p = (Player) e.getEntity();
        final Location loc = p.getLocation();
        plugin.getBodies().put(p.getName(), loc);

        if (plugin.getConfig().getBoolean("decay", false)) {
            plugin.deSpawn(p.getName());
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onEntityDamage(EntityDamageEvent event){
        if(!(event.getEntity() instanceof Player)){
            return;
        }
        Player player = (Player)event.getEntity();
        if(plugin.inGracePeriod(player.getName())){
            event.setCancelled(true);
        }
    }
}
