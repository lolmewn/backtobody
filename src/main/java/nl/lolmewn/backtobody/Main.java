package nl.lolmewn.backtobody;

import net.milkbowl.vault.economy.Economy;
import org.bstats.bukkit.Metrics;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.HashSet;

public class Main extends JavaPlugin {

    private final HashMap<String, Location> bodies = new HashMap<>();
    private final HashSet<String> grace = new HashSet<>();
    private Economy eco;

    @Override
    public void onDisable() {
        getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        getLogger().info("v" + this.getDescription().getVersion() + " has been enabled.");
        new Metrics(this);
    }

    void deSpawn(final String playerName) {
        getServer().getScheduler().runTaskLater(this, () -> bodies.remove(playerName),
                20 * this.getConfig().getInt("decay-time", 120));
    }

    HashMap<String, Location> getBodies() {
        return bodies;
    }
    
    boolean inGracePeriod(String name){
        return this.grace.contains(name);
    }

    private boolean initVault() {
        if (this.eco != null) {
            return true;
        }
        if (this.getServer().getPluginManager().getPlugin("Vault") == null) {
            //Vault not found
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = this.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            //Vault not found
            return false;
        }
        this.eco = rsp.getProvider();
        return true;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Must be a player to perform this command!");
            return false;
        }
        final Player player = ((Player) sender);
        final String name = player.getName();
        // Permission check
        if (!player.hasPermission("backtobody.btb")) {
            player.sendMessage(ChatColor.RED + cmd.getPermissionMessage());
            return true;
        }
        // They didn't die yet, so they can't go back to their body
        if (!this.bodies.containsKey(player.getName())) {
            player.sendMessage(ChatColor.RED + "You haven't died yet! Can't go back to your body!");
            return true;
        }
        final double cost = getConfig().getDouble("cost");
        if (cost != 0 && this.initVault()) {
            if (!eco.has(player, cost)) {
                player.sendMessage(ChatColor.RED + "You don't have enough money to go back to your body! Money needed: " + cost);
                return true;
            }
            this.eco.withdrawPlayer(player, cost);
            player.sendMessage(ChatColor.GREEN + "Paid " + cost + " to return to your body.");
        }
        player.teleport(this.bodies.get(player.getName()));
        player.sendMessage(ChatColor.GREEN + "You return to your body...");
        bodies.remove(name);
        if(this.getConfig().contains("grace-period")){
            this.grace.add(name);
            this.getServer().getScheduler().runTaskLater(this, () -> grace.remove(name),
                    20 * this.getConfig().getInt("grace-period", 0));
        }
        return true;
    }
}
